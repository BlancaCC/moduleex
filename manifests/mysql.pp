class mymodule::mysql{
	###mySQL server
	class { 'mysql::server':}

	mysql::db { 'mpwar':
	    user     => 'vagrant',
	    password => 'mpwardb',
	}
	mysql::db { 'mpwar_test':
	    user     => 'vagrant',
	    password => 'mpwardb',
	}
}
