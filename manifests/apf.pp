class mymodule::apf{

  #APACHE
  class { 'apache': } 

  #############PHP 5.6
  include ::yum::repo::remi
  package { 'libzip-last':
    require => Yumrepo['remi']
  }

  $php_version = '56'

  # remi_php55 requires the remi repo as well
  if $php_version == '55' {
    $yum_repo = 'remi-php55'
    include ::yum::repo::remi_php55
  }
  # remi_php56 requires the remi repo as well
  elsif $php_version == '56' {
    $yum_repo = 'remi-php56'
    class{'::yum::repo::remi_php56':
      require => Package['libzip-last']
          }
  }
  # version 5.4
  elsif $php_version == '54' {
    $yum_repo = 'remi'
    include ::yum::repo::remi
  }

  class { 'php':
    version => 'latest',
      require => Yumrepo[$yum_repo],
  }

  php::module { [ 'devel', 'pear', 'mbstring', 'xml', 'pecl-memcache', 'soap' ]: }

  #Create files
  file { '/var/www/myproject/index.php':
    ensure  => 'present',
     # $name => 'CentOS',
     # $version => '7',
    source  => "puppet:///modules/mymodule/index.php",
    mode    => '0644',
  }

  file { '/var/www/myproject/info.php':
    ensure  => 'present',
    content => "<?php phpinfo();",
    mode    => '0644',
  }
}