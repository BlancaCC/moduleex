# Class: myModule
# ===========================
#
# Full description of class myModule here.
#
# Parameters
# ----------
#
#
class mymodule { 

host { 'localhost':
    ensure       => 'present',
    target       => '/etc/hosts',
    ip           => '127.0.0.1',
    host_aliases => ['mysql']
  }

# Miscellaneous packages.
$misc_packages = [
  'sendmail','vim-enhanced','telnet','zip','unzip','screen',
  'libssh2','libssh2-devel','gcc','gcc-c++','autoconf','automake','postgresql-libs']

 package { $misc_packages: ensure => latest }

#my module

include mymodule::apf
include mymodule::mysql
include mymodule::mdb


# Ensure Time Zone and Region.
class { 'timezone':
  timezone => 'Europe/Madrid',
}

#NTP
class { '::ntp':
  server => [ '1.es.pool.ntp.org', '2.europe.pool.ntp.org', '3.europe.pool.ntp.org' ],
}

#VH
apache::vhost { 'myMpwqe.prod':
    port    => '80',
    docroot       => '/var/www/myproject',
  }

  apache::vhost { 'pmyMpwar.dev':
    port    => '80',
    docroot       => '/var/www/project1',
  }

# Ip Tables.
  if $operatingsystemrelease == '7.0.1406'
  {
    # firewalld - Centos 7
    firewalld_rich_rule { 'Accept HTTP':
      ensure  => present,
      zone    => 'public',
      service => 'http',
      action  => 'accept',
    }
  }
  else
  {
    package { 'iptables':
      ensure => present,
      before => File['/etc/sysconfig/iptables'],
    }
    file { '/etc/sysconfig/iptables':
      ensure  => file,
      owner   => "root",
      group   => "root",
      mode    => 600,
      replace => true,
      source  => "puppet:///modules/mysite16/iptables.txt",
    }
    service { 'iptables':
      ensure     => running,
      enable     => true,
      subscribe  => File['/etc/sysconfig/iptables'],
    }
  }
    

}
